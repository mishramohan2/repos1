from django.contrib import admin
from polls.models import Choice,Poll


class ChoiceInline(admin.TabularInline):
    model=Choice
    extra=2

class Polladmin(admin.ModelAdmin):
    fieldsets = [
        (None,{'fields':['question']}),
	('PUBLICATION',{'fields':['pub_date'],'classes':['collapse']}),
    ]
    inlines=[ChoiceInline]	
    list_display=('question','pub_date','was_published_recently')
    list_filter=['pub_date']
    search_fields=['question']
admin.site.register(Poll,Polladmin)
# Register your models here.


