from django.db import models
from django.utils import timezone
import datetime

# Create your models here.
class Todos(models.Model):
    name=models.CharField(max_length=100,blank=True)
    description=models.TextField()
    created=models.DateTimeField('created on')
    
    def __unicode__(self):
        return self.name	
