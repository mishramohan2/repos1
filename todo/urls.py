from django.conf.urls import patterns,url

from todo import views

urlpatterns=patterns('',
    url(r'^delete/$',views.delete,name='delete'),
    url(r'^add/$',views.add,name='add'),
    url(r'^$',views.index,name='index'),
    url(r'^(?P<t_id>\d+)/$',views.desc,name='desc')
)
