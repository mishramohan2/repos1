from django.contrib import admin
from todo.models import Todos

class todosadmin(admin.ModelAdmin):
    fieldsets=[
        (None,{'fields':['name']}),
        ('Description',{'fields':['description'],'classes':['collapse']}),
        ('Date Info',{'fields':['created'],'classes':['collapse']}),
    ]
    list_display=('name','description','created')
    search_fields=['name']
# Register your models here.
admin.site.register(Todos,todosadmin)
